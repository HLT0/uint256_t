/*
uint256_t.h
An unsigned 256 bit integer library for C++

Copyright (c) 2013 - 2017 Jason Lee @ calccrypto at gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

With much help from Auston Sterling

Thanks to Fran�ois Dessenne for convincing me
to do a general rewrite of this class.
*/

#ifndef __UINT256_T__
#define __UINT256_T__

#include <climits>
#include <cmath>
#include <cstdint>
#include <limits>
#include <ostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include "uint128_t.h"

#ifndef ENABLE_IF_Z
#if __cplusplus >= 201402L   // C++ 14
#define ENABLE_IF_Z(T) std::enable_if_t <std::is_integral<T>::value>
#elif __cplusplus >= 201103L // C++11
#define ENABLE_IF_Z(T) typename std::enable_if <std::is_integral<T>::value, T>::type
#else
#error uint256_t requires at least C++11 to compile
#endif
#endif

class uint256_t;

// Give uint256_t type traits
namespace std {  // This is probably not a good idea
    template <> struct is_arithmetic <uint256_t> : std::true_type {};
    template <> struct is_integral   <uint256_t> : std::true_type {};
    template <> struct is_unsigned   <uint256_t> : std::true_type {};
};

class uint256_t{
    private:
        uint128_t UPPER, LOWER;

    public:
        // Constructors
        constexpr uint256_t()
            : UPPER(), LOWER()
        {}

        constexpr uint256_t(const uint256_t & rhs)
            : UPPER(rhs.UPPER), LOWER(rhs.LOWER)
        {}

        constexpr uint256_t(uint256_t && rhs)
            : UPPER(std::move(rhs.UPPER)), LOWER(std::move(rhs.LOWER))
        {
            if (this != &rhs){
                rhs.UPPER = 0;
                rhs.LOWER = 0;
            }
        }

        template <typename T, typename = ENABLE_IF_Z(T)>
        constexpr uint256_t(const T & rhs)
            : UPPER(0), LOWER(rhs)
        {}

        template <typename S, typename T, typename = ENABLE_IF_Z(S), typename = ENABLE_IF_Z(T)>
        constexpr uint256_t(const S & upper_rhs, const T & lower_rhs)
            : UPPER(upper_rhs), LOWER(lower_rhs)
        {}

        template <typename R, typename S, typename T, typename U,
                  typename = ENABLE_IF_Z(R),
                  typename = ENABLE_IF_Z(S),
                  typename = ENABLE_IF_Z(T),
                  typename = ENABLE_IF_Z(U)>
        constexpr uint256_t(const R & upper_lhs, const S & lower_lhs, const T & upper_rhs, const U & lower_rhs)
            : UPPER(upper_lhs, lower_lhs), LOWER(upper_rhs, lower_rhs)
        {}

        //  RHS input args only

        // Assignment Operator
        uint256_t & operator=(const uint256_t & rhs);
        uint256_t & operator=(uint256_t && rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator=(const T & rhs){
            UPPER = 0;
            LOWER = rhs;
            return *this;
        }

        // Typecast Operators
        operator bool      () const;
        operator uint8_t   () const;
        operator uint16_t  () const;
        operator uint32_t  () const;
        operator uint64_t  () const;
        operator uint128_t () const;

        // Bitwise Operators
        uint256_t operator&(const uint128_t & rhs) const;
        uint256_t operator&(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator&(const T & rhs) const{
            return uint256_t(0, LOWER & (uint128_t) rhs);
        }

        uint256_t & operator&=(const uint128_t & rhs);
        uint256_t & operator&=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator&=(const T & rhs){
            UPPER = 0;
            LOWER &= rhs;
            return *this;
        }

        uint256_t operator|(const uint128_t & rhs) const;
        uint256_t operator|(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator|(const T & rhs) const{
            return uint256_t(UPPER, LOWER | uint128_t(rhs));
        }

        uint256_t & operator|=(const uint128_t & rhs);
        uint256_t & operator|=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator|=(const T & rhs){
            LOWER |= (uint128_t) rhs;
            return *this;
        }

        uint256_t operator^(const uint128_t & rhs) const;
        uint256_t operator^(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator^(const T & rhs) const{
            return uint256_t(UPPER, LOWER ^ (uint128_t) rhs);
        }

        uint256_t & operator^=(const uint128_t & rhs);
        uint256_t & operator^=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator^=(const T & rhs){
            LOWER ^= (uint128_t) rhs;
            return *this;
        }

        uint256_t operator~() const;

        // Bit Shift Operators
        uint256_t operator<<(const uint128_t & shift) const;
        uint256_t operator<<(const uint256_t & shift) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator<<(const T & rhs) const{
            return *this << uint256_t(rhs);
        }

        uint256_t & operator<<=(const uint128_t & shift);
        uint256_t & operator<<=(const uint256_t & shift);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator<<=(const T & rhs){
            *this = *this << uint256_t(rhs);
            return *this;
        }

        uint256_t operator>>(const uint128_t & shift) const;
        uint256_t operator>>(const uint256_t & shift) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator>>(const T & rhs) const{
            return *this >> uint256_t(rhs);
        }

        uint256_t & operator>>=(const uint128_t & shift);
        uint256_t & operator>>=(const uint256_t & shift);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator>>=(const T & rhs){
            *this = *this >> uint256_t(rhs);
            return *this;
        }

        // Logical Operators
        bool operator!() const;

        bool operator&&(const uint128_t & rhs) const;
        bool operator&&(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator&&(const T & rhs) const{
            return ((bool) *this && rhs);
        }

        bool operator||(const uint128_t & rhs) const;
        bool operator||(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator||(const T & rhs) const{
            return ((bool) *this || rhs);
        }

        // Comparison Operators
        bool operator==(const uint128_t & rhs) const;
        bool operator==(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator==(const T & rhs) const{
            return (!UPPER && (LOWER == uint128_t(rhs)));
        }

        bool operator!=(const uint128_t & rhs) const;
        bool operator!=(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator!=(const T & rhs) const{
            return ((bool) UPPER | (LOWER != uint128_t(rhs)));
        }

        bool operator>(const uint128_t & rhs) const;
        bool operator>(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator>(const T & rhs) const{
            return ((bool) UPPER | (LOWER > uint128_t(rhs)));
        }

        bool operator<(const uint128_t & rhs) const;
        bool operator<(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator<(const T & rhs) const{
            return (!UPPER)?(LOWER < uint128_t(rhs)):false;
        }

        bool operator>=(const uint128_t & rhs) const;
        bool operator>=(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator>=(const T & rhs) const{
            return ((*this > rhs) | (*this == rhs));
        }

        bool operator<=(const uint128_t & rhs) const;
        bool operator<=(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        bool operator<=(const T & rhs) const{
            return ((*this < rhs) | (*this == rhs));
        }

        // Arithmetic Operators
        uint256_t operator+(const uint128_t & rhs) const;
        uint256_t operator+(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator+(const T & rhs) const{
            return uint256_t(UPPER + ((LOWER + (uint128_t) rhs) < LOWER), LOWER + (uint128_t) rhs);
        }

        uint256_t & operator+=(const uint128_t & rhs);
        uint256_t & operator+=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator+=(const T & rhs){
            UPPER = UPPER + ((LOWER + rhs) < LOWER);
            LOWER = LOWER + rhs;
            return *this;
        }

        uint256_t operator-(const uint128_t & rhs) const;
        uint256_t operator-(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator-(const T & rhs) const{
            return uint256_t(UPPER - ((LOWER - rhs) > LOWER), LOWER - rhs);
        }

        uint256_t & operator-=(const uint128_t & rhs);
        uint256_t & operator-=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator-=(const T & rhs){
            *this = *this - rhs;
            return *this;
        }

        uint256_t operator*(const uint128_t & rhs) const;
        uint256_t operator*(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator*(const T & rhs) const{
            return *this * uint256_t(rhs);
        }

        uint256_t & operator*=(const uint128_t & rhs);
        uint256_t & operator*=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator*=(const T & rhs){
            *this = *this * uint256_t(rhs);
            return *this;
        }

    private:
        std::pair <uint256_t, uint256_t> divmod(const uint256_t & lhs, const uint256_t & rhs) const;

    public:
        uint256_t operator/(const uint128_t & rhs) const;
        uint256_t operator/(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator/(const T & rhs) const{
            return *this / uint256_t(rhs);
        }

        uint256_t & operator/=(const uint128_t & rhs);
        uint256_t & operator/=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator/=(const T & rhs){
            *this = *this / uint256_t(rhs);
            return *this;
        }

        uint256_t operator%(const uint128_t & rhs) const;
        uint256_t operator%(const uint256_t & rhs) const;

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t operator%(const T & rhs) const{
            return *this % uint256_t(rhs);
        }

        uint256_t & operator%=(const uint128_t & rhs);
        uint256_t & operator%=(const uint256_t & rhs);

        template <typename T, typename = ENABLE_IF_Z(T)>
        uint256_t & operator%=(const T & rhs){
            *this = *this % uint256_t(rhs);
            return *this;
        }

        // Increment Operators
        uint256_t & operator++();
        uint256_t operator++(int);

        // Decrement Operators
        uint256_t & operator--();
        uint256_t operator--(int);

        // Nothing done since promotion doesn't work here
        uint256_t operator+() const;

        // two's complement
        uint256_t operator-() const;

        // Get private values
        const uint128_t & upper() const;
        const uint128_t & lower() const;

        // Get bitsize of value
        uint16_t bits() const;

        // Get string representation of value
        std::string str(uint8_t base = 10, const unsigned int & len = 0) const;
};

// Bitwise Operators
uint256_t operator&(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
 uint256_t operator&(const T & lhs, const uint256_t & rhs){
    return rhs & lhs;
}

uint128_t & operator&=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator&=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (rhs & lhs);
}

uint256_t operator|(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator|(const T & lhs, const uint256_t & rhs){
    return rhs | lhs;
}

uint128_t & operator|=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator|=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (rhs | lhs);
}

uint256_t operator^(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator^(const T & lhs, const uint256_t & rhs){
    return rhs ^ lhs;
}

uint128_t & operator^=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator^=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (rhs ^ lhs);
}

// Bitshift operators
uint256_t operator<<(const bool      & lhs, const uint256_t & rhs);
uint256_t operator<<(const uint8_t   & lhs, const uint256_t & rhs);
uint256_t operator<<(const uint16_t  & lhs, const uint256_t & rhs);
uint256_t operator<<(const uint32_t  & lhs, const uint256_t & rhs);
uint256_t operator<<(const uint64_t  & lhs, const uint256_t & rhs);
uint256_t operator<<(const uint128_t & lhs, const uint256_t & rhs);
uint256_t operator<<(const int8_t    & lhs, const uint256_t & rhs);
uint256_t operator<<(const int16_t   & lhs, const uint256_t & rhs);
uint256_t operator<<(const int32_t   & lhs, const uint256_t & rhs);
uint256_t operator<<(const int64_t   & lhs, const uint256_t & rhs);

uint128_t & operator<<=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator<<=(T & lhs, const uint256_t & rhs){
    lhs = static_cast <T> (uint256_t(lhs) << rhs);
    return lhs;
}

uint256_t operator>>(const bool      & lhs, const uint256_t & rhs);
uint256_t operator>>(const uint8_t   & lhs, const uint256_t & rhs);
uint256_t operator>>(const uint16_t  & lhs, const uint256_t & rhs);
uint256_t operator>>(const uint32_t  & lhs, const uint256_t & rhs);
uint256_t operator>>(const uint64_t  & lhs, const uint256_t & rhs);
uint256_t operator>>(const uint128_t & lhs, const uint256_t & rhs);
uint256_t operator>>(const int8_t    & lhs, const uint256_t & rhs);
uint256_t operator>>(const int16_t   & lhs, const uint256_t & rhs);
uint256_t operator>>(const int32_t   & lhs, const uint256_t & rhs);
uint256_t operator>>(const int64_t   & lhs, const uint256_t & rhs);

uint128_t & operator>>=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator>>=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (uint256_t(lhs) >> rhs);
}

// Comparison Operators
bool operator==(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator==(const T & lhs, const uint256_t & rhs){
    return (!rhs.upper() && ((uint64_t) lhs == rhs.lower()));
}

bool operator!=(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator!=(const T & lhs, const uint256_t & rhs){
    return (rhs.upper() | ((uint64_t) lhs != rhs.lower()));
}

bool operator>(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator>(const T & lhs, const uint256_t & rhs){
    return rhs.upper()?false:((uint128_t) lhs > rhs.lower());
}

bool operator<(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator<(const T & lhs, const uint256_t & rhs){
    return rhs.upper()?true:((uint128_t) lhs < rhs.lower());
}

bool operator>=(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator>=(const T & lhs, const uint256_t & rhs){
    return rhs.upper()?false:((uint128_t) lhs >= rhs.lower());
}

bool operator<=(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
bool operator<=(const T & lhs, const uint256_t & rhs){
    return rhs.upper()?true:((uint128_t) lhs <= rhs.lower());
}

// Arithmetic Operators
uint256_t operator+(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator+(const T & lhs, const uint256_t & rhs){
    return rhs + lhs;
}

uint128_t & operator+=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator+=(T & lhs, const uint256_t & rhs){
    lhs = static_cast <T> (rhs + lhs);
    return lhs;
}

uint256_t operator-(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator-(const T & lhs, const uint256_t & rhs){
    return -(rhs - lhs);
}

uint128_t & operator-=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator-=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (-(rhs - lhs));
}

uint256_t operator*(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator*(const T & lhs, const uint256_t & rhs){
    return rhs * lhs;
}

uint128_t & operator*=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator*=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (rhs * lhs);
}

uint256_t operator/(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator/(const T & lhs, const uint256_t & rhs){
    return uint256_t(lhs) / rhs;
}

uint128_t & operator/=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator/=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (uint256_t(lhs) / rhs);
}

uint256_t operator%(const uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
uint256_t operator%(const T & lhs, const uint256_t & rhs){
    return uint256_t(lhs) % rhs;
}

uint128_t & operator%=(uint128_t & lhs, const uint256_t & rhs);

template <typename T, typename = ENABLE_IF_Z(T)>
T & operator%=(T & lhs, const uint256_t & rhs){
    return lhs = static_cast <T> (uint256_t(lhs) % rhs);
}

// IO Operator
std::ostream & operator<<(std::ostream & stream, const uint256_t & rhs);

#if __cplusplus >= 201402L
// Give uint256_t numeric limits
namespace std {  // This is probably not a good idea
    template <>
    class numeric_limits <uint256_t> {
        public:
            static constexpr bool is_specialized = true;
            static constexpr bool is_signed = false;
            static constexpr bool is_integer = true;
            static constexpr bool is_exact = true;
            static constexpr bool has_infinity = false;
            static constexpr bool has_quiet_NaN = false;
            static constexpr bool has_signaling_NaN = false;
            static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
            static constexpr bool has_denorm_loss = false;
            static constexpr std::float_round_style round_style = std::round_toward_zero;
            static constexpr bool is_iec559 = false;
            static constexpr bool is_bounded = true;
            static constexpr bool is_modulo = true;
            static constexpr int digits = CHAR_BIT*(sizeof(uint64_t) + sizeof(uint64_t));
            static constexpr int digits10 = std::numeric_limits<uint256_t>::digits * std::log10(2);
            static constexpr int max_digits10 = 0;
            static constexpr int radix = 2;
            static constexpr int min_exponent = 0;
            static constexpr bool min_exponent10 = 0;
            static constexpr int max_exponent = 0;
            static constexpr bool max_exponent10 = 0;
            static constexpr bool traps = true;
            static constexpr bool tinyness_before = false;

            static constexpr uint256_t min() {
                return 0;
            }

            static constexpr uint256_t lowest() {
                return 0;
            }

            static constexpr uint256_t max() {
                return uint256_t((uint64_t) 0xffffffffffffffffULL, (uint64_t) 0xffffffffffffffffULL,
                                 (uint64_t) 0xffffffffffffffffULL, (uint64_t) 0xffffffffffffffffULL);
            }

            static constexpr uint256_t epsilon() {
                return 0;
            }

            static constexpr uint256_t rounding_error() {
                return 0;
            }

            static constexpr uint256_t infinity() {
                return 0;
            }

            static constexpr uint256_t quiet_NaN() {
                return 0;
            }

            static constexpr uint256_t signaling_NaN() {
                return 0;
            }

            static constexpr uint256_t denorm_min() {
                return 0;
            }
    };
}
#endif

#endif
